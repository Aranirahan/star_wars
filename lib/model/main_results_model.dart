import 'package:star_wars/model/people_model.dart';

class MainResultPeople {
  List<People> _peoples;

  MainResultPeople.fromJson(Map<String, dynamic> parsedJson) {
    List<People> peoples = [];

    for (int i = 0; i < parsedJson['results'].length; i++) {
      People result = People.fromJson(parsedJson['results'][i]);
      peoples.add(result);
    }
    _peoples = peoples;
  }

  List<People> get peoples => _peoples;

  set peoples(List<People> value) {
    _peoples = value;
  }
}
