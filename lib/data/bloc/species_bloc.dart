import 'package:rxdart/rxdart.dart';
import 'package:star_wars/data/persistence/repository.dart';
import 'package:star_wars/model/species_model.dart';

class SpeciesBloc {
  Repository _repository = Repository();

  final _speciesFetcher = PublishSubject<Species>();

  Observable<Species> get species => _speciesFetcher.stream;

  fetchSpecies(String urlSpecies) async {
    Species species = await _repository.fetchSpecies(urlSpecies);
    _speciesFetcher.sink.add(species);
  }

  dispose() {
    _speciesFetcher.close();
  }
}

final speciesBloc = SpeciesBloc();
