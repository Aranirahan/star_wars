class Film {
  String title;
  int episodeId;
  String openingCrawl;
  String director;
  String producer;
  String releaseDate;

  Film({
    this.title,
    this.episodeId,
    this.openingCrawl,
    this.director,
    this.producer,
    this.releaseDate,
  });

  factory Film.fromJson(Map<String, dynamic> json) => new Film(
        title: json["title"],
        episodeId: json["episode_id"],
        openingCrawl: json["opening_crawl"],
        director: json["director"],
        producer: json["producer"],
        releaseDate: json["release_date"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "episode_id": episodeId,
        "opening_crawl": openingCrawl,
        "director": director,
        "producer": producer,
        "release_date": releaseDate,
      };
}
