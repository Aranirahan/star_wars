class Species {
  String name;
  String classification;
  String designation;
  String skinColors;
  String hairColors;
  String eyeColors;
  String language;
  String averageHeight;
  String averageLifespan;

  Species({
    this.name,
    this.classification,
    this.designation,
    this.skinColors,
    this.hairColors,
    this.eyeColors,
    this.language,
    this.averageHeight,
    this.averageLifespan,
  });

  factory Species.fromJson(Map<String, dynamic> json) => new Species(
        name: json["name"],
        classification: json["classification"],
        designation: json["designation"],
        skinColors: json["skin_colors"],
        hairColors: json["hair_colors"],
        eyeColors: json["eye_colors"],
        language: json["language"],
        averageHeight: json["average_height"],
        averageLifespan: json["average_lifespan"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "classification": classification,
        "designation": designation,
        "skin_colors": skinColors,
        "hair_colors": hairColors,
        "eye_colors": eyeColors,
        "language": language,
        "average_height": averageHeight,
        "average_lifespan": averageLifespan,
      };
}
