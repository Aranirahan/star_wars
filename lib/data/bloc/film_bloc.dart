import 'package:rxdart/rxdart.dart';
import 'package:star_wars/data/persistence/repository.dart';
import 'package:star_wars/model/film_model.dart';

class FilmBloc {
  Repository _repository = Repository();

  final _filmFetcher = PublishSubject<Film>();

  Observable<Film> get film => _filmFetcher.stream;

  fetchFilm(String urlFilm) async {
    Film film = await _repository.fetchFilm(urlFilm);
    _filmFetcher.sink.add(film);
  }

  dispose() {
    _filmFetcher.close();
  }
}

final filmBloc = FilmBloc();
