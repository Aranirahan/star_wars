import 'package:rxdart/rxdart.dart';
import 'package:star_wars/data/persistence/repository.dart';
import 'package:star_wars/model/main_results_model.dart';

class PeopleBloc {
  Repository _repository = Repository();

  final _peopleFetcher = PublishSubject<MainResultPeople>();

  Observable<MainResultPeople> get mainResultPeople => _peopleFetcher.stream;

  fetchMainResultPeople() async {
    MainResultPeople mainResultPeople =
        await _repository.fetchMainResultPeople();
    _peopleFetcher.sink.add(mainResultPeople);
  }

  dispose() {
    _peopleFetcher.close();
  }
}

final peopleBloc = PeopleBloc();
