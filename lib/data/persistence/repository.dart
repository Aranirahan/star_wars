import 'package:star_wars/model/film_model.dart';
import 'package:star_wars/model/main_results_model.dart';
import 'package:star_wars/model/species_model.dart';

import 'api_provider.dart';

class Repository {
  ApiProvider appApiProvider = ApiProvider();

  Future<MainResultPeople> fetchMainResultPeople() =>
      appApiProvider.fetchMainResultPeople();

  Future<Film> fetchFilm(String urlFilm) => appApiProvider.fetchFilm(urlFilm);

  Future<Species> fetchSpecies(String urlSpecies) =>
      appApiProvider.fetchSpecies(urlSpecies);
}
