import 'package:flutter/material.dart';
import 'package:star_wars/data/bloc/film_bloc.dart';
import 'package:star_wars/model/film_model.dart';

class ViewDetailFilmPage extends StatefulWidget {
  ViewDetailFilmPage({Key key, this.filmUrl}) : super(key: key);

  final String filmUrl;

  @override
  ViewDetailFilmState createState() => ViewDetailFilmState();
}

class ViewDetailFilmState extends State<ViewDetailFilmPage> {
  @override
  Widget build(BuildContext context) {
    filmBloc.fetchFilm(widget.filmUrl);
    return Scaffold(
      appBar: AppBar(title: Text('Detail Film')),
      body: StreamBuilder<Film>(
        stream: filmBloc.film,
        builder: (BuildContext context, AsyncSnapshot<Film> snapshot) {
          if (snapshot.hasData) {
            Film film = snapshot.data;
            return SingleChildScrollView(
              child: Column(children: [
                Container(
                  padding: EdgeInsets.only(top: 10.0),
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.title,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Title',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.episodeId.toString(),
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Episode',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.openingCrawl,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Opening Crawl',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.director,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Director',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.producer,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Producer',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: film.releaseDate,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Release Date',
                    ),
                  ),
                ),
              ]),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
