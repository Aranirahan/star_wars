import 'package:flutter/material.dart';
import 'package:star_wars/data/bloc/species_bloc.dart';
import 'package:star_wars/model/species_model.dart';

class ViewDetailSpeciesPage extends StatefulWidget {
  ViewDetailSpeciesPage({Key key, this.speciesUrl}) : super(key: key);

  final String speciesUrl;

  @override
  ViewDetailSpeciesState createState() => ViewDetailSpeciesState();
}

class ViewDetailSpeciesState extends State<ViewDetailSpeciesPage> {
  @override
  Widget build(BuildContext context) {
    speciesBloc.fetchSpecies(widget.speciesUrl);
    return Scaffold(
      appBar: AppBar(title: Text('Detail Film')),
      body: StreamBuilder<Species>(
        stream: speciesBloc.species,
        builder: (BuildContext context, AsyncSnapshot<Species> snapshot) {
          if (snapshot.hasData) {
            Species species = snapshot.data;
            return SingleChildScrollView(
              child: Column(children: [
                Container(
                  padding: EdgeInsets.only(top: 10.0),
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.name,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Name',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.classification,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Classification',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.designation,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Designation',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.skinColors,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Skin Colors',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.hairColors,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Hair Colors',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.eyeColors,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Eye Colors',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.language,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Language',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.averageHeight,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Average Height',
                    ),
                  ),
                ),
                Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: TextFormField(
                    enabled: false,
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    initialValue: species.averageLifespan,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Average Lifespan',
                    ),
                  ),
                ),
              ]),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
