import 'dart:convert';

class People {
  int id;
  String name;
  String hairColor;
  String skinColor;
  String eyeColor;
  String birthYear;
  String gender;
  List<String> filmsUrls;
  List<String> speciesUrls;
  String height;
  String mass;

  People({
    this.id,
    this.name,
    this.hairColor,
    this.skinColor,
    this.eyeColor,
    this.birthYear,
    this.gender,
    this.filmsUrls,
    this.speciesUrls,
    this.height,
    this.mass,
  });

  factory People.fromJson(Map<String, dynamic> json) {
    List<String> films = [];
    for (int i = 0; i < json['films'].length; i++) {
      String result = json['films'][i];
      films.add(result);
    }

    List<String> species = [];
    for (int i = 0; i < json['species'].length; i++) {
      String result = json['species'][i];
      species.add(result);
    }

    return new People(
      id: json["id"],
      name: json["name"],
      hairColor: json["hair_color"],
      skinColor: json["skin_color"],
      eyeColor: json["eye_color"],
      birthYear: json["birth_year"],
      gender: json["gender"],
      filmsUrls: films,
      speciesUrls: species,
      height: json["height"],
      mass: json["mass"],
    );
  }

  factory People.fromJsonDb(Map<String, dynamic> json) {
    String filmsFromDb = json['films'];
    var filmsObjJson = '{"films":$filmsFromDb}';
    var tagsJson = jsonDecode(filmsObjJson)['films'];
    List<String> films = tagsJson != null ? List.from(tagsJson) : null;

    String speciesFromDb = json['species'];
    var speciesObjJson = '{"species":$speciesFromDb}';
    var speciesDecode = jsonDecode(speciesObjJson)['species'];
    List<String> species = tagsJson != null ? List.from(speciesDecode) : null;

    return new People(
      id: json["id"],
      name: json["name"],
      hairColor: json["hair_color"],
      skinColor: json["skin_color"],
      eyeColor: json["eye_color"],
      birthYear: json["birth_year"],
      gender: json["gender"],
      filmsUrls: films,
      speciesUrls: species,
      height: json["height"],
      mass: json["mass"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "hair_color": hairColor,
        "skin_color": skinColor,
        "eye_color": eyeColor,
        "birth_year": birthYear,
        "gender": gender,
        "films": jsonEncode(filmsUrls),
        "species": jsonEncode(speciesUrls),
        "height": height,
        "mass": mass,
      };
}
