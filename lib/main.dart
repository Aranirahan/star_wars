import 'package:flutter/material.dart';
import 'package:star_wars/data/bloc/bloc_provider.dart';
import 'package:star_wars/data/bloc/local_people_bloc.dart';
import 'package:star_wars/data/bloc/people_bloc.dart';
import 'package:star_wars/data/bloc/detail_people_bloc.dart';
import 'package:star_wars/model/main_results_model.dart';
import 'package:star_wars/model/people_model.dart';
import 'package:star_wars/ui/view_detail_people.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Star Wars App',
      theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
      home: BlocProvider(
        bloc: LocalPeopleBloc(),
        child: PeoplePage(title: 'Star Wars App'),
      ),
    );
  }
}

class PeoplePage extends StatefulWidget {
  PeoplePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PeoplePageState createState() => _PeoplePageState();
}

class _PeoplePageState extends State<PeoplePage> {
  LocalPeopleBloc _localPeopleBloc;
  bool isAsc = true;

  @override
  void initState() {
    super.initState();

    _localPeopleBloc = BlocProvider.of<LocalPeopleBloc>(context);
  }

  void _addPeople(People people) async {
    _localPeopleBloc.inAddPeople.add(people);
  }

  void _navigateToViewPeople(People people, bool isNew) async {
    bool update = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => BlocProvider(
          bloc: DetailPeopleBloc(),
          child: ViewDetailPeoplePage(
            people: people,
            isNew: isNew,
          ),
        ),
      ),
    );

    if (update != null) {
      if (isAsc) {
        _localPeopleBloc.getPeoplesAsc();
      } else {
        _localPeopleBloc.getPeoplesDesc();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    peopleBloc.fetchMainResultPeople();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.sort),
              onPressed: () {
                isAsc = !isAsc;
                if (isAsc) {
                  _localPeopleBloc.getPeoplesAsc();
                } else {
                  _localPeopleBloc.getPeoplesDesc();
                }
              }),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: StreamBuilder(
                stream: _localPeopleBloc.peoples,
                builder: (context, AsyncSnapshot<List<People>> snapshotLocal) {
                  if (snapshotLocal.hasError) {
                    return Text(snapshotLocal.error.toString());
                  }
                  if (snapshotLocal.hasData) {
                    if (snapshotLocal.data.length == 0) {
                      return StreamBuilder<MainResultPeople>(
                        stream: peopleBloc.mainResultPeople,
                        builder: (BuildContext context,
                            AsyncSnapshot<MainResultPeople> snapshot) {
                          if (snapshot.hasData) {
                            List<People> peoples = snapshot.data.peoples;
                            for (int i = 0; i < peoples.length; i++) {
                              _addPeople(peoples[i]);
                            }

                            return listViewPeopleBuilder(peoples);
                          }

                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      );
                    }
                    List<People> localPeoples = snapshotLocal.data;
                    return listViewPeopleBuilder(localPeoples);
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          People people = People(filmsUrls: [], speciesUrls: []);
          _navigateToViewPeople(people, true);
        },
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        child: Icon(Icons.add),
      ),
    );
  }

  ListView listViewPeopleBuilder(List<People> peoples) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: peoples.length,
      itemBuilder: (BuildContext context, int index) {
        People people = peoples[index];

        return GestureDetector(
          onTap: () {
            _navigateToViewPeople(people, false);
          },
          child: makeCard(people.name, people.gender),
        );
      },
    );
  }
}

Card makeCard(String name, String gender) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(name, gender),
      ),
    );

ListTile makeListTile(String name, String gender) => ListTile(
    contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
    leading: Container(
      padding: EdgeInsets.only(right: 12.0),
      decoration: new BoxDecoration(
          border: new Border(
              right: new BorderSide(width: 1.0, color: Colors.white24))),
      child: Icon(Icons.person, color: Colors.white),
    ),
    title: Text(
      name,
      style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    ),
    subtitle: Row(
      children: <Widget>[
        Icon(Icons.linear_scale, color: Colors.yellowAccent),
        Text(gender, style: TextStyle(color: Colors.white))
      ],
    ),
    trailing:
        Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0));
