import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:star_wars/model/film_model.dart';
import 'package:star_wars/model/main_results_model.dart';
import 'package:star_wars/model/species_model.dart';

class ApiProvider {
  Client client = Client();

  final _baseUrlPeople = "https://swapi.co/api/people/?format=json";

  Future<MainResultPeople> fetchMainResultPeople() async {
    final response = await client.get("$_baseUrlPeople");

    if (response.statusCode == 200) {
      return MainResultPeople.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load person');
    }
  }

  Future<Film> fetchFilm(String urlFilm) async {
    final response = await client.get("$urlFilm");

    if (response.statusCode == 200) {
      return Film.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load film');
    }
  }

  Future<Species> fetchSpecies(String urlSpecies) async {
    final response = await client.get("$urlSpecies");

    if (response.statusCode == 200) {
      return Species.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load species');
    }
  }
}
