import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:star_wars/data/bloc/bloc_provider.dart';
import 'package:star_wars/data/bloc/detail_people_bloc.dart';
import 'package:star_wars/model/people_model.dart';
import 'package:star_wars/ui/view_detail_film.dart';
import 'package:star_wars/ui/view_detail_species.dart';

class ViewDetailPeoplePage extends StatefulWidget {
  ViewDetailPeoplePage({Key key, this.people, this.isNew}) : super(key: key);

  final People people;
  final bool isNew;

  @override
  _ViewDetailPeoplePageState createState() => _ViewDetailPeoplePageState();
}

class _ViewDetailPeoplePageState extends State<ViewDetailPeoplePage> {
  DetailPeopleBloc _detailPeopleBloc;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _hairColorController = new TextEditingController();
  TextEditingController _skinColorController = new TextEditingController();
  TextEditingController _eyeColorController = new TextEditingController();
  TextEditingController _birthYearController = new TextEditingController();
  TextEditingController _genderController = new TextEditingController();
  TextEditingController _heightController = new TextEditingController();
  TextEditingController _massController = new TextEditingController();

  @override
  void initState() {
    super.initState();

    _detailPeopleBloc = BlocProvider.of<DetailPeopleBloc>(context);

    _nameController.text = widget.people.name;
    _hairColorController.text = widget.people.hairColor;
    _skinColorController.text = widget.people.skinColor;
    _eyeColorController.text = widget.people.eyeColor;
    _birthYearController.text = widget.people.birthYear;
    _genderController.text = widget.people.gender;
    _heightController.text = widget.people.height;
    _massController.text = widget.people.mass;
  }

  void updateField() {
    //name cant be empty
    String nameCapital =
        '${_nameController.text[0].toUpperCase()}${_nameController.text.substring(1)}';

    widget.people.name = nameCapital;
    widget.people.hairColor = _hairColorController.text;
    widget.people.skinColor = _skinColorController.text;
    widget.people.eyeColor = _eyeColorController.text;
    widget.people.birthYear = _birthYearController.text;
    widget.people.gender = _genderController.text;
    widget.people.height = _heightController.text;
    widget.people.mass = _massController.text;
  }

  void _savePeople() async {
    updateField();

    _detailPeopleBloc.inSavePeople.add(widget.people);

    _detailPeopleBloc.saved.listen((saved) {
      if (saved) {
        Navigator.of(context).pop(true);
      }
    });
  }

  void _updatePeople() async {
    updateField();

    _detailPeopleBloc.inUpdatePeople.add(widget.people);

    _detailPeopleBloc.updated.listen((updated) {
      if (updated) {
        Navigator.of(context).pop(true);
      }
    });
  }

  void _deletePeople() {
    _detailPeopleBloc.inDeletePeople.add(widget.people.id);

    _detailPeopleBloc.deleted.listen((deleted) {
      if (deleted) {
        Navigator.of(context).pop(true);
      }
    });
  }

  Future navigateToFilmPage(context, String filmUrl) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ViewDetailFilmPage(filmUrl: filmUrl)));
  }

  Future navigateToSpeciesPage(context, String speciesUrl) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ViewDetailSpeciesPage(speciesUrl: speciesUrl)));
  }

  Future<void> _neverSatisfied() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Are you sure to delete?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Yupe'),
              onPressed: () {
                _deletePeople();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.isNew ? "Add People" : "Detail People"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: (widget.isNew) ? _savePeople : _updatePeople,
          ),
          IconButton(
            icon: Icon(Icons.delete),
//            onPressed: _deletePeople,
            onPressed: _neverSatisfied,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            padding: EdgeInsets.only(top: 10.0),
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Name',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _hairColorController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Hair Color',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _skinColorController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Skin Color',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _eyeColorController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Eye Color',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _birthYearController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Birth Year',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
              maxLines: null,
              controller: _genderController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Gender',
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
                keyboardType: TextInputType.number,
                maxLines: null,
                controller: _heightController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Height',
                ),
                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly]),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: TextField(
                keyboardType: TextInputType.number,
                maxLines: null,
                controller: _massController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Mass',
                ),
                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly]),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Visibility(
              visible: !widget.isNew,
              child: Column(children: [
                Text("${widget.people.filmsUrls.length ?? 0} Film Urls",
                    style: TextStyle(
                      fontSize: 18,
                    )),
                ListView.builder(
                  padding: EdgeInsets.only(top: 10.0),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: widget.people.filmsUrls.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                    String filmUrl = widget.people.filmsUrls[index];

                    return GestureDetector(
                      onTap: () {
                        navigateToFilmPage(context, filmUrl);
                      },
                      child: Center(
                        child: Container(
                          height: 40,
                          child: Text(
                            filmUrl,
                            style: TextStyle(
                                fontSize: 18,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ]),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Visibility(
              visible: !widget.isNew,
              child: Column(children: [
                Text("${widget.people.speciesUrls.length ?? 0} Species Urls",
                    style: TextStyle(
                      fontSize: 18,
                    )),
                ListView.builder(
                  padding: EdgeInsets.only(top: 10.0),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: widget.people.speciesUrls.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                    String speciesUrl = widget.people.speciesUrls[index];

                    return GestureDetector(
                      onTap: () {
                        navigateToSpeciesPage(context, speciesUrl);
                      },
                      child: Center(
                        child: Container(
                          height: 40,
                          child: Text(
                            speciesUrl,
                            style: TextStyle(
                                fontSize: 18,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ]),
            ),
          ),
        ]),
      ),
    );
  }
}
