import 'dart:async';

import 'package:star_wars/data/bloc/bloc_provider.dart';
import 'package:star_wars/data/database.dart';
import 'package:star_wars/model/people_model.dart';

class LocalPeopleBloc implements BlocBase {
  // Create a broadcast controller that allows this stream to be listened
  // to multiple times. This is the primary, if not only, type of stream you'll be using.
  final _peoplesController = StreamController<List<People>>.broadcast();

  // Input stream. We add our peoples to the stream using this variable.
  StreamSink<List<People>> get _inPeoples => _peoplesController.sink;

  // Output stream. This one will be used within our pages to display the peoples.
  Stream<List<People>> get peoples => _peoplesController.stream;

  // Input stream for adding new peoples. We'll call this from our pages.
  final _addPeopleController = StreamController<People>.broadcast();

  StreamSink<People> get inAddPeople => _addPeopleController.sink;

  LocalPeopleBloc() {
    // Retrieve all the Peoples on initialization
    getPeoplesAsc();

    // Listens for changes to the addPeopleController and calls _handleAddPeople on change
    _addPeopleController.stream.listen(_handleAddPeople);
  }

  // All stream controllers you create should be closed within this function
  @override
  void dispose() {
    _peoplesController.close();
    _addPeopleController.close();
  }

  void getPeoplesAsc() async {
    List<People> peoples = await DBProvider.db.getPeoples();

    peoples.sort((a, b) => a.name.compareTo(b.name));
    _inPeoples.add(peoples);
  }

  void getPeoplesDesc() async {
    List<People> peoples = await DBProvider.db.getPeoples();

    peoples.sort((a, b) => b.name.compareTo(a.name));
    _inPeoples.add(peoples);
  }

  void _handleAddPeople(People people) async {
    // Create the people in the database
    await DBProvider.db.newPeople(people);

    // Retrieve all the peoples again after one is added.
    // This allows our pages to update properly and display the
    // newly added people.
    getPeoplesAsc();
  }
}
