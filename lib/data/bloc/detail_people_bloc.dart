import 'dart:async';

import 'package:star_wars/data/bloc/bloc_provider.dart';
import 'package:star_wars/data/database.dart';
import 'package:star_wars/model/people_model.dart';

class DetailPeopleBloc implements BlocBase {
  final _savePeopleController = StreamController<People>.broadcast();

  StreamSink<People> get inSavePeople => _savePeopleController.sink;

  final _updatePeopleController = StreamController<People>.broadcast();

  StreamSink<People> get inUpdatePeople => _updatePeopleController.sink;

  final _deletePeopleController = StreamController<int>.broadcast();

  StreamSink<int> get inDeletePeople => _deletePeopleController.sink;

  final _peopleSaveController = StreamController<bool>.broadcast();

  StreamSink<bool> get _inSaved => _peopleSaveController.sink;

  Stream<bool> get saved => _peopleSaveController.stream;

  final _peopleUpdateController = StreamController<bool>.broadcast();

  StreamSink<bool> get _inUpdateed => _peopleUpdateController.sink;

  Stream<bool> get updated => _peopleUpdateController.stream;

  final _peopleDeletedController = StreamController<bool>.broadcast();

  StreamSink<bool> get _inDeleted => _peopleDeletedController.sink;

  Stream<bool> get deleted => _peopleDeletedController.stream;

  DetailPeopleBloc() {
    // Listen for changes to the stream, and execute a function when a change is made
    _savePeopleController.stream.listen(_handleSavePeople);
    _updatePeopleController.stream.listen(_handleUpdatePeople);
    _deletePeopleController.stream.listen(_handleDeletePeople);
  }

  @override
  void dispose() {
    _savePeopleController.close();
    _updatePeopleController.close();
    _deletePeopleController.close();

    _peopleSaveController.close();
    _peopleUpdateController.close();
    _peopleDeletedController.close();
  }

  void _handleSavePeople(People people) async {
    await DBProvider.db.newPeople(people);

    _inSaved.add(true);
  }

  void _handleUpdatePeople(People people) async {
    await DBProvider.db.updatePeople(people);

    _inUpdateed.add(true);
  }

  void _handleDeletePeople(int id) async {
    await DBProvider.db.deletePeople(id);

    _inDeleted.add(true);
  }
}
