import 'dart:io';
import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:star_wars/model/people_model.dart';

class DBProvider {
  // Create a singleton
  DBProvider._();

  static final DBProvider db = DBProvider._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDir = await getApplicationDocumentsDirectory();
    String path = join(documentsDir.path, 'star-wars-app.db');

    return await openDatabase(path, version: 1, onOpen: (db) async {},
        onCreate: (Database db, int version) async {
      // Create the People table
      await db.execute('''
				CREATE TABLE people(
					id INTEGER PRIMARY KEY,
					name TEXT DEFAULT '',
					hair_color TEXT DEFAULT '',
					skin_color TEXT DEFAULT '',
					eye_color TEXT DEFAULT '',
					birth_year TEXT DEFAULT '',
					gender TEXT DEFAULT '',
					films TEXT DEFAULT '',
					species TEXT DEFAULT '',
					height TEXT DEFAULT '',
					mass TEXT DEFAULT ''
				)
			''');
    });
  }

  /*
	 * People Table
	 */
  newPeople(People people) async {
    final db = await database;
    var res = await db.insert('people', people.toJson());

    return res;
  }

  getPeoples() async {
    final db = await database;
    var res = await db.query('people');
    List<People> peoples = res.isNotEmpty
        ? res.map((people) => People.fromJsonDb(people)).toList()
        : [];

    return peoples;
  }

  getPeople(int id) async {
    final db = await database;
    var res = await db.query('people', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty ? People.fromJsonDb(res.first) : null;
  }

  updatePeople(People people) async {
    final db = await database;
    var res = await db.update('people', people.toJson(),
        where: 'id = ?', whereArgs: [people.id]);

    return res;
  }

  deletePeople(int id) async {
    final db = await database;

    db.delete('people', where: 'id = ?', whereArgs: [id]);
  }
}
